﻿﻿using FullMessagePostingApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace FullMessagePostingApp.Controllers {
    
    [ApiController]
    [Route("api")]
    public class MainController : ControllerBase {
        private readonly MessagesService messagesService;
        private readonly TriviaService triviaService;

        public MainController(MessagesService messagesService, TriviaService triviaService) {
            this.messagesService = messagesService;
            this.triviaService = triviaService;
        }

        [HttpGet("json-msg")]
        //GET /api/json-msg
        public IActionResult GetJsonMessages() {
            return new JsonResult(messagesService.GetMessages());
        }
        
        [HttpGet("json-resultados")]
        //GET /api/json-resultados
        public IActionResult GetJsonResultados() {
            return new JsonResult(triviaService.TriviaResultsBoard);
        }
        
        [HttpGet("iot-msg")]
        //GET /api/json-msg
        public ContentResult GetIoTMessages() {
            return new ContentResult() {
                Content = messagesService.GetIoTMessages()
            };
        }
    }
}