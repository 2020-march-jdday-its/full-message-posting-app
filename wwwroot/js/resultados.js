﻿﻿$(document).ready(function() {
    getMessages();
    setInterval(getMessages, 1000);
});

function getMessages() {
    $.ajax({
        type: "GET",
        url: "/api/json-resultados",
        dataType: "text"
    })
        .done(function(response) {
            $("#divResults").html(buildResultadosList(response));
        })
        .fail(function( $xhr ) {

        });//END Ajax call
}


function buildResultadosList(resultadosJson) {
    var html = '';
    var alertStyles = ['success', 'warning', 'danger', 'primary', 'secondary'];
    if (resultadosJson) {
        var resultados = JSON.parse(resultadosJson);
        var si = 0;
        for (var i = 0; i < resultados.length; i++) {
            var r = resultados[i];
            html += '<div class="row">';
            html += ' <div class="col-lg-12">';
            html += '  <div class="alert alert-' + alertStyles[si] + '" role="alert">';
            html += '   <h2 class="alert-heading">' + (i+1) + '° Lugar</h2>';
            html += '   <h4 class="alert-heading">' + r.team + '</h4>';
            html += '   <p>Respuestas acertadas: ' + r.note + '</p>';
            html += '   <p>TimeStamp: ' + r.timeStamp + '</p>';
            html += '  </div>';
            html += ' </div>';
            html += '</div>';

            if (si >= alertStyles.length) {
                si = 0;
            } else {
                si++;
            }
        }
    }
    return html;
}