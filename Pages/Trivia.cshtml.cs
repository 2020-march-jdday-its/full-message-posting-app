﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using System.Linq;
 using FullMessagePostingApp.Model;
using FullMessagePostingApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FullMessagePostingApp.Pages {
    public class TriviaModel : PageModel {
        
        public string ErrorMessage { get; set; }
        public IList<TriviaItem> TriviaItems { get; set; }
        private readonly TriviaService triviaService;

        [BindProperty]
        public string Equipo { get; set; }
        
        [BindProperty]
        public List<Response> Responses { get; set; }
        
        public TriviaModel(TriviaService triviaService) {
            this.triviaService = triviaService;
            TriviaItems = triviaService.TriviaItems;
        }
        
        public IActionResult OnGet() {
            return Page();
        }

        public IActionResult OnPost() {
            if (string.IsNullOrEmpty(Equipo) || Equipo.Equals("-1")) {
                ErrorMessage = "Selecciona un Equipo";
                return Page();
            }

            var firstFoundEmptyTriviaItem = Responses.FirstOrDefault(i => i.Answer == -1);
            if (firstFoundEmptyTriviaItem != null) {
                ErrorMessage = "Debes seleccionar una respuesta para todas las preguntas";
                return Page();
            }
            
            triviaService.AddTriviaResponseFromTeam(Responses, Equipo);

            return RedirectToPage("Resultados");
        }
    }
}
