﻿﻿using System.ComponentModel.DataAnnotations;
using FullMessagePostingApp.Model;
using FullMessagePostingApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FullMessagePostingApp.Pages {
    public class AdminModel : PageModel {

        private readonly MessagesService messagesService;
        private readonly TriviaService triviaService;

        public AdminModel(MessagesService messagesService, TriviaService triviaService) {
            this.messagesService = messagesService;
            this.triviaService = triviaService;
        }
        
        public IActionResult OnGet() {
            return Page();
        }

        public IActionResult OnPostCleanMessages() {
            messagesService.CleanMessages();
            return Page();
        }
        
        public IActionResult OnPostResetTrivia() {
            triviaService.ResetTrivia();
            return Page();
        }
    }
}
