﻿﻿using System.ComponentModel.DataAnnotations;
using FullMessagePostingApp.Model;
using FullMessagePostingApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FullMessagePostingApp.Pages {
    public class IndexModel : PageModel {

        private readonly MessagesService messagesService;
        
        [BindProperty]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(16, MinimumLength = 3)]
        public string Name { get; set; }
        
        [BindProperty]
        [Required(ErrorMessage = "Message is required")]
        [StringLength(16, MinimumLength = 3)]
        public string Message { get; set; }

        public IndexModel(MessagesService _messagesService) {
            this.messagesService = _messagesService;
        }
        
        public IActionResult OnGet() {
            return Page();
        }

        public IActionResult OnPost() {
            if (!ModelState.IsValid) {
                return Page();
            }

            var msg = new Message {
                Name = Name,
                Msg = Message
            };
            
            messagesService.AddMessage(msg);
            
            return Page();
        }
    }
}
