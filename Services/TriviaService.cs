﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FullMessagePostingApp.Model;
using Newtonsoft.Json;

namespace FullMessagePostingApp.Services {
    public class TriviaService {
        public IList<TriviaItem> TriviaItems { get; private set; }
        
        public Dictionary<string, TriviaResult> TriviaResultsByTeam { get; set; }
        
        // This List represents the final scores of the participants
        // The list is ordered, so the first element is the winner, the next one is the second place and so forth
        public List<TriviaResult> TriviaResultsBoard { get; set; }

        public TriviaService() {
            LoadTriviaItems();
            ResetTrivia();
        }

        public void ResetTrivia() {
            TriviaResultsByTeam = new Dictionary<string, TriviaResult>();
            TriviaResultsBoard = new List<TriviaResult>();
        }

        public void AddTriviaResponseFromTeam(List<Response> responses, string team) {
            if (TriviaResultsByTeam.ContainsKey(team)) {
                // Team already sent responses, just overwrite them
            } else {// Add responses of the team to the results board
                TriviaResultsByTeam.Add(team, CalculateNote(responses, team));
            }

            CalculateTriviaResults();
        }

        // Gets the Note for the provided Responses object
        // It represents the number of questions answered correctly
        private TriviaResult CalculateNote(List<Response> responses, string team) {
            var triviaResult = new TriviaResult();
            int note = 0;
            if (responses != null && responses.Count > 0 && TriviaItems != null && TriviaItems.Count > 0) {
                foreach (var response in responses) {
                    var correctAnswer = TriviaItems.FirstOrDefault(item => item.Id == response.TriviaItemId);
                    if (correctAnswer != null) {
                        if (correctAnswer.Answer == response.Answer) {
                            response.IsCorrect = true;
                            note++; 
                        }
                    }
                }                
            }

            triviaResult.Note = note;
            triviaResult.Responses = responses;
            triviaResult.Team = team;
            
            return triviaResult;
        }

        private void CalculateTriviaResults() {
            TriviaResultsBoard = TriviaResultsByTeam
                .Select(dicItem => dicItem.Value)
                .OrderByDescending(item => item.Note)
                .ThenBy(item => item.TimeStamp)
                .ToList();
        }

        private void LoadTriviaItems() {
            string filePath = "Trivia.json";
            using (var triviaItemsFile = new StreamReader(filePath)) {
                TriviaItems = JsonConvert.DeserializeObject<List<TriviaItem>>(triviaItemsFile.ReadToEnd());
            }
        }
        
    }
}