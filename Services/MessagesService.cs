﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using FullMessagePostingApp.Model;

namespace FullMessagePostingApp.Services {
    public class MessagesService {

        private readonly CensoringService censoringService;
        private Queue<Message> Messages;
        private Timer _timer;
        
        public MessagesService(CensoringService _censoringService) {
            Messages = new Queue<Message>();
            this.censoringService = _censoringService;
            //Tick every 30sec
            _timer = new Timer(TimerTask, null, 1000, 30000);
        }

        public void CleanMessages() {
            Messages = new Queue<Message>();
        }

        public void AddMessage(Message message) {
            if (Messages.Count >= 50) {
                //Max 50 messages allowed
                Messages.Dequeue();
            }
            if (censoringService.ContainsCensoredWords(message.Name)
                || censoringService.ContainsCensoredWords(message.Msg)) {
                //Not enqueue bad messages
                return;
            }
            Messages.Enqueue(message);
        }

        public IEnumerable GetMessages() {
            return Messages;
        }

        public string GetIoTMessages() {
            var builder = new StringBuilder();
            foreach (var msg in Messages) {
                builder.Append(msg.Name)
                    .Append(":")
                    .Append(msg.Msg)
                    .Append(";");
            }
            return builder.ToString();
        }

        private void TimerTask(object timerState) {
            if (Messages.Count > 0) {
                //Delete one message
                Console.WriteLine(Messages.Dequeue());
            }
        }
        
    }
}