﻿namespace FullMessagePostingApp.Model {
    public class Response {
        public int TriviaItemId { get; set; }
        public int Answer { get; set; }
        public bool IsCorrect { get; set; }
    }
}