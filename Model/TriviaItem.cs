﻿namespace FullMessagePostingApp.Model {
    public class TriviaItem {
        public int Id { get; set; }
        public string Question { get; set; }
        public string[] Options { get; set; }
        public int Answer { get; set; }
    }
}