﻿using System;
using System.Collections.Generic;

namespace FullMessagePostingApp.Model {
    public class TriviaResult {

        public TriviaResult() {
            TimeStamp = DateTime.Now;
        }
        public string Team { get; set; }
        public int Note { get; set; }
        public List<Response> Responses { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}